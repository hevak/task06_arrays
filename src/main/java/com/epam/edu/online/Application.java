package com.epam.edu.online;

import com.epam.edu.online.task1.container.ViewContainer;
import com.epam.edu.online.task1.game.view.ViewHeroGame;
import com.epam.edu.online.task1.logical.ViewArrays;
import com.epam.edu.online.task1.queue.ViewPriorityQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private final static Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        new ViewPriorityQueue().runTaskPriorityQueue();
        new ViewContainer().runTaskPriorityQueue();
        new ViewArrays().runTaskArray();
        new ViewHeroGame().startGame();


    }
}
