package com.epam.edu.online.task1.container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;

public class ViewContainer {
    private final static Logger LOGGER = LogManager.getLogger(ViewContainer.class);

    public void runTaskPriorityQueue() {
        Container<String> container = new Container<>();
        container.set("Ssss");
        String t = container.get();
        LOGGER.debug(t);
        container.setListNumbers(new ArrayList<Integer>(Arrays.asList(
                2,3,5,456
        )));
        container.printListNumbers();
    }
}
