package com.epam.edu.online.task1.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;

public class ViewPriorityQueue {
    private final static Logger LOGGER = LogManager.getLogger(ViewPriorityQueue.class);

    public void runTaskPriorityQueue() {
        Queue<String> queue = new MyPriorityQueue<>();
        // offer some items
        queue.offer("asd");
        queue.offer("asd3");
        queue.offer("asd2");
        //iter elements
        for (String s : queue) {
            LOGGER.debug("-" + s);
        }
        // remove last element from queue
        LOGGER.debug("Was removed - " + queue.poll());
        // return first element
        LOGGER.debug("Size: " + queue.size());
        LOGGER.debug(queue.toString());
        // peek item
        LOGGER.debug("peek - " + queue.peek());
    }
}
