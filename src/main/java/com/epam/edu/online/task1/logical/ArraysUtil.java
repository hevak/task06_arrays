package com.epam.edu.online.task1.logical;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.ArrayUtils;

public class ArraysUtil {

    private final static Logger LOGGER = LogManager.getLogger(ArrayUtils.class);


    public int[] uniqueNumbersInRow(int arr[]) {
        int[] result = arr;
        for (int i = 0, j = i + 1; i < arr.length; i++, j++) {
            if (j < arr.length && arr[i] == arr[j]) {
                result = removeElementInRow(result);
            }
        }
        return result;
    }


    public int[] uniqueElements(int arr[]) {
        int counterSize = 0;
        int[] result = new int[arr.length];
        for (int i = 0, j; i < arr.length; i++) {
            for (j = 0; j < i; j++) {
                if (arr[i] == arr[j]) {
                    break;
                }
            }
            if (i == j) {
                result[i] = arr[i];
            }
        }
        for (int i = 0; i < result.length; i++) {
            if (result[i] != 0) {
                counterSize++;
            }
        }
        return getFinishArray(result, counterSize);
    }


    public int[] uniqueElements(int[] firstArray, int[] secondArray) {
        int[] result = firstArray;
        int countSize = 0;
        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {
                if (firstArray[i] == secondArray[j]) {
                    result = removeElement(result, firstArray[i]);
                    countSize++;
                }
            }
        }
        int[] uniqueArray = new int[result.length - countSize];
        for (int i = 0; i < uniqueArray.length; i++) {
            uniqueArray[i] = result[i];
        }
        return uniqueArray;
    }

    //    private int[] removeElement2(int[] arr, int element) {
//
//    }
    private int[] removeElementInRow(int[] arr) {
        int[] result = new int[arr.length];
        int count = 0;
        int temp = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]!=0) count++;
            if (arr[i] != temp) {
                result[i] = arr[i];
            }
            temp = arr[i];
        }
        return getFinishArray(result, count);
    }

    private int[] getFinishArray(int[] result, int count) {
        int[] uniqueArray = new int[count];
        for (int i = 0, j = 0; i < result.length; i++) {
            if (result[i] != 0) {
                uniqueArray[j] = result[i];
                j++;
            }
        }
        return uniqueArray;
    }

    private int[] removeElement(int[] uniqueArray, int element) {
        int[] result = new int[uniqueArray.length];
        for (int i = 0; i < uniqueArray.length; i++) {
            if (uniqueArray[i] != element) {
                result[i] = uniqueArray[i];
            }
        }
        return result;
    }

    public int[] sameElements(int[] firstArray, int[] secondArray) {
        int[] result = new int[greaterLenght(firstArray, secondArray)];
        int countSize = 0;
        for (int i : firstArray) {
            for (int j : secondArray) {
                if (i == j) {
                    result[countSize] = i;
                    countSize++;
                }
            }
        }
        int[] thirdArray = new int[countSize];
        for (int i = 0; i < thirdArray.length; i++) {
            thirdArray[i] = result[i];
        }
        return thirdArray;
    }

    private int greaterLenght(int[] firstArray, int[] secondArray) {
        return firstArray.length > secondArray.length ? firstArray.length : secondArray.length;
    }


}
