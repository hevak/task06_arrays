package com.epam.edu.online.task1.game.model;

import java.util.Random;

public class Artefact extends Power {
    private String name = "artefact-";
    private static int count = 0;
    public Artefact() {
        setPower(new Random().nextInt(70) + 10);
        count++;
    }
    public String getName() {
        return name+count;
    }

}
