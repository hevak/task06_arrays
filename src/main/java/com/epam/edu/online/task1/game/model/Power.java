package com.epam.edu.online.task1.game.model;

public class Power {
    private int power;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
