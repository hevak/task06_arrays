package com.epam.edu.online.task1.game.model;

public class Hero extends Power {
    private final int POWER = 25;
    public Hero() {
        setPower(POWER);
    }

    public int addPower(int power) {
        setPower(getPower() + power);
        return getPower();
    }
    public int removePower(int power) {
        setPower(getPower() - power);
        return getPower();
    }
}
