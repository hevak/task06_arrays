package com.epam.edu.online.task1.game.view;

import com.epam.edu.online.task1.game.model.Artefact;
import com.epam.edu.online.task1.game.model.Hero;
import com.epam.edu.online.task1.game.model.Monster;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class ViewHeroGame {
    private final static Logger LOGGER = LogManager.getLogger(ViewHeroGame.class);

    private int COUNT_DOOR = 10;

    public void startGame() {
        Hero hero = new Hero();
        for (int door = 1; door <= COUNT_DOOR; door++) {
            openDoor(hero, door);
        }
        LOGGER.info("Hero are win with "+hero.getPower()+" power");
    }

    private void openDoor(Hero hero, int door) {
        boolean isGoodDoor = new Random().nextBoolean();
        LOGGER.info("Open " + door + " door");
        String power = "power";
        if (isGoodDoor) {
            Artefact artefact = new Artefact();
            LOGGER.info(artefact.getName() + " " + artefact.getPower() + power);
            hero.addPower(artefact.getPower());
            LOGGER.info("Hero power: " + hero.getPower() + power);
        } else {
            Monster monster = new Monster();
            LOGGER.info(monster.getName() + " " + monster.getPower() + power);
            hero.removePower(monster.getPower());
            LOGGER.info("Hero power: " + hero.getPower() + power);
            if (hero.getPower() < 0) {
                LOGGER.fatal("Hero is dead");
                System.exit(0);
            }
        }
    }

}
