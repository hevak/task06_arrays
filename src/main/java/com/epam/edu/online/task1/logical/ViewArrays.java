package com.epam.edu.online.task1.logical;

import com.epam.edu.online.task1.container.ViewContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewArrays {
    private final static Logger LOGGER = LogManager.getLogger(ViewContainer.class);

    private int[] firstArray = new int[]{1, 2, 3, 3, 4, 4, 2,2,3};
    private int[] secondArray = new int[]{5, 6, 7, 8, 9, 10, 11, 12};
    private ArraysUtil arraysUtil = new ArraysUtil();

    public void runTaskArray() {
        int[] sameElements = arraysUtil.sameElements(firstArray, secondArray);
        for (Integer integer : sameElements) {
            LOGGER.info(integer);
        }
//
        int[] uniqueElements = arraysUtil.uniqueElements(firstArray, secondArray);
        for (Integer integer : uniqueElements) {
            LOGGER.info(integer);
        }
//
        int[] unique = arraysUtil.uniqueElements(firstArray);
        for (int i:unique){
            LOGGER.info(i);
        }

        int[] uniqueNumbersInRow = arraysUtil.uniqueNumbersInRow(firstArray);
        for (Integer integer : uniqueNumbersInRow) {
            LOGGER.info(integer);
        }
    }


}
