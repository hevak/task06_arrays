package com.epam.edu.online.task1.game.model;

import java.util.Random;

public class Monster extends Power {
    private String name = "monster-";
    private static int count = 0;
    public Monster() {
        setPower(new Random().nextInt(95) + 5);
        count++;
    }
    public String getName() {
        return name+count;
    }
}
