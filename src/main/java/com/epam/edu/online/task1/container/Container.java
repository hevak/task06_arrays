package com.epam.edu.online.task1.container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Container<T> {
    private final static Logger LOGGER = LogManager.getLogger(Container.class);
    private T t;

    public Container() {
    }
    public Container(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public void set(T t) {
        this.t = t;
    }

    private List<? extends Number> listNumbers;
    public void setListNumbers(List<? extends Number> listNumbers) {
        this.listNumbers = listNumbers;
    }
    public void printListNumbers() {
        listNumbers.forEach(number -> LOGGER.info(number));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Container<?> container = (Container<?>) o;

        return t != null ? t.equals(container.t) : container.t == null;
    }

    @Override
    public int hashCode() {
        return t != null ? t.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Container{" +
                "t=" + t +
                '}';
    }
}
