package com.epam.edu.online.task1.queue;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringJoiner;

public class MyPriorityQueue<E> extends AbstractQueue<E> {
    private static final int DEFAULT_INITIAL_CAPACITY = 8;

    private Object[] queue;

    public MyPriorityQueue() {
        this.queue = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public MyPriorityQueue(int copacity) {
        this.queue = new Object[copacity];
    }
    private int position = 0;

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            @Override
            public boolean hasNext() {
                if (position< indexLastElement)
                    return true;
                else
                    return false;
            }

            @Override
            public E next() {
                if (this.hasNext())
                    return (E) queue[position++];
                else
                    return null;
            }
        };
    }

    @Override
    public int size() {
        return indexLastElement;
    }

    private int indexLastElement = 0;
    @Override
    public boolean offer(E e) {
        if (e == null) {
            return false;
        }
        for (int i = 0; i < queue.length; i++) {
            if (queue[i] == null) {
                queue[i] = e;
                indexLastElement++;
                return true;
            }
        }
        increaseSize();
        offer(e);
        return false;//todo bug
    }

    private void increaseSize() {
        Object[] objects = new Object[queue.length * (2)];
        copyData(objects);
    }

    private void copyData(Object[] objects) {
        for (int i = 0; i < queue.length; i++) {
            objects[i] = queue[i];
        }
        this.queue = objects;
    }

    @Override
    public E  poll() {
        Object o = queue[indexLastElement - 1];
        queue[indexLastElement-1] = null;
        indexLastElement--;
        return (E) o;
    }

    @Override
    public E peek() {
        for (int i = 0; i < indexLastElement - 1; i++) {
            if(queue[i]==null) continue;
            return (E) queue[i];
        }
        return null;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ");
        for (int i = 0; i < indexLastElement; i++) {
            joiner.add(queue[i].toString());
        }
        return "[" + joiner.toString() + "]";
    }
}
